<?php

$plugin = array(
  'title' => t('ECK Entity Form'),
  'single' => TRUE,
  'description' => t('Add an ECK entity form.'),
  'category' => t('ECK'),
  'required context' => new ctools_context_optional(t('Entity'), 'entity'),
);

/**
 * ECK form config form.
 */
function eck_ctools_eck_form_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // List of options for selecting entity type and bundle.
  $options = array();
  foreach (EntityType::loadAll() as $entity_type) {
    foreach (Bundle::loadByEntityType($entity_type) as $bundle) {
      $key = $entity_type->name . "|" . $bundle->name;
      $val = $entity_type->name . " | " . $bundle->name;
      $options[$key] = $val;
    }
  }

  // Select element for selecting entity type and bundle.
  $form['eck'] = array(
    '#title' => t('Entity type/bundle'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['eck'],
  );
  $form['eck']['#states']['visible'][':input[name="edit-context"]'] = array('value' => '');

  return $form;
}

/**
 * ECK form config form submit handler.
 */
function eck_ctools_eck_form_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['eck'] = $form_state['values']['eck'];
}

/**
 * ECK form render callback.
 */
function eck_ctools_eck_form_content_type_render($subtype, $conf, $panel_args, $context) {

  list($entity_type, $entity, $bundle, $op) = _eck_ctools_eck_form_content_type_prepare($conf, $context);

  $entity_types = array_keys(EntityType::loadAll());
  if (!in_array($entity_type, $entity_types)) {
    // Configured for use on a non-ECK entity. Fail!
    return;
  }

  $block = new stdClass();
  $block->title = _eck_ctools_eck_form_content_type_title($entity_type, $bundle,  $entity, $op);
  $block->content = drupal_get_form("eck__entity__form_{$op}_{$entity_type}_{$bundle}", $entity);

  return $block;
}

/**
 * ECK form administrative info callback.
 */
function eck_ctools_eck_form_content_type_admin_info($plugin, $conf, $contexts) {

  $context = reset($contexts);
  list($entity_type, $entity, $bundle, $op) = _eck_ctools_eck_form_content_type_prepare($conf, $context);

  $block = new stdClass();

  $entity_types = array_keys(EntityType::loadAll());
  if (!in_array($entity_type, $entity_types)) {
    // Configured for use on a non-ECK entity. Fail!
    $block->title = t('Please reconfigure for use on an ECK entity.');
    $block->content = t('OMG! What do you think you\'re doing?');
    return $block;
  }

  $block->title = _eck_ctools_eck_form_content_type_title($entity_type, $bundle,  $entity, $op);
  $block->content = array();

  $data[t('Entity Type')] = $entity_type;
  if (!empty($entity->id)) {
    $data[t('Entity ID')] = $entity->id;
  }
  $data[t('Bundle')] = $bundle;
  if (!empty($entity->title)) {
    $data[t('Title')] = $entity->title;
  }

  foreach ($data as $key => $val) {
    $block->content[] = "<strong>$key:</strong> $val";
  }

  $block->content = implode('<br />', $block->content);

  return $block;
}

/**
 * Pull a few things from the plugin configuration and context.
 */
function _eck_ctools_eck_form_content_type_prepare($conf, $context) {
  // Figure out what we're doing.
  if (empty($context->data)) {
    // Add.
    list($entity_type, $bundle) = explode('|',  $conf['eck'], 2);
    $entity = entity_create($entity_type, array('type' => $bundle));
    $op = 'add';
  } else {
    // Edit.
    $parts = explode(':', $context->plugin);
    $entity_type = array_pop($parts);
    $entity = $context->data;
    list(, , $bundle) = entity_extract_ids($entity_type, $entity);
    $op = 'edit';
  }

  return array($entity_type, $entity, $bundle, $op);
}

/**
 * Plugin title generation.
 */
function _eck_ctools_eck_form_content_type_title($entity_type, $bundle,  $entity, $op) {
  // A few bits of info about entities and bundles.
  $info = entity_get_info($entity_type);
  $label = !empty($info['bundles'][$bundle]['label']) ? $info['bundles'][$bundle]['label'] : $info['label'];

  if ($op == 'add') {
    // $info['bundles'][$bundle]['label']
    $title = t('Add') . ' ' . $label;
  } else {
    $w = entity_metadata_wrapper($entity_type, $entity);
    $title = $w->title->value();
    $title = t('Edit') . ' ' . (!empty($title) ? $title : $label);
  }

  return $title;
}
